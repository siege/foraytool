<h1 id="foray_user_documentation">Foray User Documentation</h1>

<p>Author: Drew McCormack<br />
Email: <a href="mailto:drewmccormack@mac.com">drewmccormack@mac.com</a><br />
Web Sites: <a href="http://www.macresearch.org">www.macresearch.org</a>, <a href="http://www.maccoremac.com">www.maccoremac.com</a>, 
           <a href="http://www.macanics.net">www.macanics.net</a></p>

<p>Fortran 90 can include reasonably complex dependencies, which must be taken into account when building a multiple-file program. Unfortunately, most build tools either don&#8217;t support Fortran, or don&#8217;t help the developer much. A standard <code>make</code> file, for example, requires you to enter dependencies manually, or develop a script to do it for you. </p>

<p>Foray is a build tool designed specifically for Fortran projects. It can be applied to anything from a small utility program with tens of files to a million line monster. Foray natively handles Fortran dependencies, takes care of locating files in subdirectories, and includes advanced features like multi-threading for better performance on multi-core systems, and multiple build configurations (<em>eg</em> debug, release, serial, parallel).</p>

<p>This document will introduce you to Foray, and give instructions for configuring it to work with your Fortran project.</p>

<h2 id="what8217s_the_problem">What&#8217;s the Problem?</h2>

<p>There are lots of build tools that support C-based languages, but very few that can handle Fortran. And yet, if anything, determining the file dependencies and build order of a Fortran 90 program is more difficult than for a C program. Where dependencies arise in C via source files with <code>#include</code> directives, a single Fortran 90 file can include multiple modules, each of which can be &#8216;used&#8217; by other files, creating build order dependencies. Add to that that most Fortran compilers generate module files in addition to object files, and you have a reasonably complicated soup to digest.</p>

<h2 id="what_options_are_available">What Options are Available?</h2>

<p>Most Fortran developers stick to <code>make</code> for their building needs, but <code>make</code> is an old tool, and doesn&#8217;t have any direct support for Fortran. You either fill in the dependencies by hand, which is error prone, or you use a script to generate the dependencies. This works, but is less than ideal, and there are other reasons not to use <code>make</code>, which will be discussed more below.</p>

<p>A more up-to-date option is <a href="http://www.scons.org/">SCons</a>. SCons is an advanced build system, and has support for Fortran, in addition to many other languages. SCons is a very worthy tool, but can be difficult to configure for Fortran. Foray is not designed to be as configurable as Scons; instead, it is an attempt to create an easy-to-use tool specifically for Fortran programs, which includes some of the advanced features of Scons.</p>

<h2 id="philosophy_and_design_requirements">Philosophy and Design Requirements</h2>

<p>Here follows a list of basic design requirements that formed the basis of Foray, and some justification for them. </p>

<p>Foray should</p>

<ul>
<li>Not be general purpose.
<ul>
<li>It should do Fortran well, and only handle enough C to get by. Java &#8212; forget it!</li>
</ul></li>
<li>Scale to millions of lines, but also be easy to use with small programs.</li>
<li>Be very simple to install, preferably just one file.
<ul>
<li>Don&#8217;t want to have to have a build tool to build your build tool!</li>
</ul></li>
<li>Favor convention over configuration.
<ul>
<li>Foray sacrifices generality for simplicity. Foray chooses a reasonable convention for how projects should be laid out, and will work in any project that is structured in that way.</li>
</ul></li>
<li>Support multiple, interdependent targets.
<ul>
<li>Large projects typically have many libraries and executables. Foray needs to handle dependencies between these targets.</li>
</ul></li>
<li>Support multiple build configurations (<em>eg</em>, debug, release, parallel, serial).</li>
<li>Work on all Unix/Linux platforms.
<ul>
<li>Sorry Windows users. We&#8217;ve never tested Foray on Windows, but we assume it doesn&#8217;t work. May not take much to get it to work though.</li>
</ul></li>
<li>Scale on multi-core systems.</li>
<li>Understand Fortran dependencies, and determine them automatically.</li>
<li>Not mix build configuration files (<em>eg</em> make files) with source files. 
<ul>
<li>All configuration should be in one file in the project root. We find this preferable to the way <code>make</code>, and even <code>scons</code>, favor recursive builds with a configuration file in every source directory. It&#8217;s analogous to how some source control tools (<em>eg.</em> CVS and Subversion) write their metadata in directories in the source tree. Our view is that tools should not mix their data directly with the source tree, and this is a philosophy shared by many recently-developed programming tools (<em>eg</em> <a href="http://git.or.cz/">Git</a>).</li>
</ul></li>
<li>Have the ability to set different compile options for different groups of files, or individual files.
<ul>
<li>Fortran compilers have bugs. It is rare that one set of compile flags work for all files in a large program. And often you will want to set higher optimization for certain performance critical files.</li>
</ul></li>
<li>Consider the compile options used to compile a file when determining if it needs recompiling.
<ul>
<li>This idea is stolen from SCons (Thanks SCons!). Often you make a change to some compiler flags for a particular subset of source files, and then need to figure out which files need &#8216;touching&#8217; so that they get recompiled. Foray stores the compile flags used for each file, and knows when they have changed, automatically rebuilding the file.</li>
</ul></li>
<li>Separate build products from source code, in a standalone directory.
<ul>
<li>Some build systems mix object files and other intermediate products through the source tree. Not good. Foray puts all build products in a standalone build directory in the project root.</li>
</ul></li>
<li>Consider a file modified if its content is modified, as well as its modification date.
<ul>
<li>Another idea taken from SCons. This can be useful if, say, you move a file aside and temporarily replace it with some other file. When you put it back again, most build systems will not rebuild the file, because the modification date of the source file is not newer than that of the object file. Foray will do a checksum, and see that the file is changed.</li>
</ul></li>
<li>Use archives in place of object files.
<ul>
<li>Build systems like <code>make</code> compare the modification date of object files to the corresponding source file to determine if a recompile is needed. This is not very robust, and results in object files being spread all over your project. Foray archives object files in static libraries, and stores time stamps in a separate database.</li>
</ul></li>
<li>Support custom preprocessors.
<ul>
<li>Many Fortran projects use the C processor, or even custom preprocessors. Foray supports preprocessors by allowing developers to write custom functions (in Python) to preprocess source files before they are compiled.</li>
</ul></li>
<li>Have a &#8216;live&#8217; configuration file.
<ul>
<li>Foray is written in Python, and you configure it for your project using a Python script called the <em>BuildInfo</em> file. In its simplest form, this involves filling values into a static data structure &mdash; you do not need to have a good grasp of Python. But if you do know how to script in Python, you can add any code you like to the BuildInfo file, and it will be executed before the build starts. Foray includes various &#8216;hooks&#8217; to perform actions at different phases of a build.</li>
</ul></li>
</ul>

<h2 id="installing_foray">Installing Foray</h2>

<p>To install Foray, just download it and ensure the <code>foray</code> tool is somewhere in your path. You will have to make sure you have Python version 2.4 or later to run it (<a href="http://www.python.org">Python web site</a>).</p>

<h2 id="foray_conventions">Foray Conventions</h2>

<p>Foray assumes all source code is below a single directory called the <em>project root</em>. Each target in the project must exist in one or more subdirectories of the project root directory. Many projects are already structured like this. For example, many projects have a directory called &#8216;src&#8217; in the project root that contains all source code. These projects already conform to Foray&#8217;s convention (provided they only have a single target). Note that there is no restriction on how deep the directory structure of each target goes; Foray will recursively search subdirectories inside a target directory.</p>

<h2 id="configuring_foray">Configuring Foray</h2>

<p>A projects build configuration is usually stored in a file called &#8216;buildinfo&#8217; in the project root directory. This file contains all the information used by Foray to build all targets.</p>

<p>The easiest way to get acquainted with a BuildInfo is to take a look at one. Here is an example BuildInfo file.</p>

<pre><code>buildinfo = \
{
    'builddir' : '$FORAY_PROJECT_ROOT/build',
    'targets' : [
        {
            'name' : 'libbase',
            'rootdirs' : ['base'],
            'buildsubdir' : 'base',
            'libraryname' : 'base',
            'skipdirs' : ['CVS', '.svn'],
            'skipfiles' : [],
            'dependson' : [],
            'compilegroups' : {
                'safe' : ['HistogramBuilder.f90']
            }
        },
        {
            'name' : 'cmc',
            'rootdirs' : ['cmc'],
            'buildsubdir' : 'cmc',
            'libraryname' : 'cmc',
            'skipdirs' : ['CVS', '.svn'],
            'skipfiles' : [],
            'dependson' : ['libbase'],
            'exename' : 'cmc',
            'mainprogramfile' : 'cmc.f90',
            'compilegroups' : {
            }
        },
        {
            'name' : 'cmctests',
            'rootdirs' : ['cmctests'],
            'buildsubdir' : 'cmctests',
            'libraryname' : 'cmctests',
            'skipdirs' : ['CVS', '.svn'],
            'skipfiles' : [],
            'dependson' : ['libbase'],
            'exename' : 'cmctests',
            'mainprogramfile' : 'cmctests.f90',
            'compilegroups' : {
            }
        }
    ],
    'defaultconfig' : 'debug',
    'configs' : [
        {
            'name' : 'default',
            'buildsubdir' : 'default',
            'compileroptions' : {
                'archivecommand'    : 'ar -r',
                'unarchivecommand'  : 'ar -d',
                'ranlibcommand'     : 'ranlib -s',
                'f77compiler'       : 'gfortran',
                'f90compiler'       : 'gfortran',
                'f77flags'          : '-c -m32 -ffixed-form',
                'f90flags'          : '-c -m32 -ffree-form',
                'modpathoption'     : '-I',
                'ccompiler'         : 'gcc',
                'cflags'            : '-c -m32 -O3 -funroll-loops -malign-natural',
                'link'              : 'gfortran',
                'linkflags'         : '',
                'prioritylibs'      : '',
                'otherlibs'         : '',
                'compilegroupflags' : {
                    'default'       : '',
                    'safe'          : '-O0'
                }
            }
        },
        {
            'name'                  : 'release',
            'inherits'              : 'default',
            'buildsubdir'           : 'release',
            'installdir'            : '$FORAY_PROJECT_ROOT/bin.release',
            'compileroptions' : {
                'compilegroupflags' : {
                    'default'       : '-O3',
                    'safe'          : '-O2'
                }
            }
        },
        {
            'name'                  : 'debug',
            'inherits'              : 'default',
            'buildsubdir'           : 'debug',
            'installdir'            : '$FORAY_PROJECT_ROOT/bin.debug',
            'compileroptions' : {
                'f77flags'          : '-c -g -m32 -ffixed-form -fbounds-check',
                'f90flags'          : '-c -g -m32 -ffree-form -fbounds-check'
            }
        }
    ]
}
</code></pre>

<p>This is a standard Python data structure; Foray expects that the variable <code>buildinfo</code> will be assigned to this structure. The data structure defines a number of settings needed by Foray:</p>

<ul>
<li>The directory used by Foray to store all intermediate build products (<code>builddir</code>).</li>
<li>The targets in the project. These can be libraries or executables (<code>targets</code>).</li>
<li>The build configurations (<code>configs</code>), and the default configuration (<code>defaultconfig</code>).</li>
</ul>

<p>Many settings in the BuildInfo file will perform shell expansions. The build directory is a case in point:</p>

<pre><code>'builddir' : '$FORAY_PROJECT_ROOT/build',
</code></pre>

<p>This environment variable <code>$FORAY_PROJECT_ROOT</code> will be substituted in determining the path to the build directory. </p>

<p>The list corresponding to the <code>targets</code> key contains target dictionaries. If the target is a library, it looks like this</p>

<pre><code>{
    'name' : 'libbase',
    'rootdirs' : ['base'],
    'buildsubdir' : 'base',
    'libraryname' : 'base',
    'skipdirs' : ['CVS', '.svn'],
    'skipfiles' : [],
    'dependson' : [],
    'compilegroups' : {
        'safe' : ['HistogramBuilder.f90']
    }
},
</code></pre>

<p>The dictionary entries are as follows:</p>

<ul>
<li><code>name</code> is the target&#8217;s name</li>
<li><code>rootdirs</code> is a list of subdirectories of the project root that holds the target&#8217;s source code</li>
<li><code>buildsubdir</code> is a subdirectory in the build directory used to store the intermediate build product files of the target; </li>
<li><code>libraryname</code> is the name of the library archive used for the target&#8217;s object files, excluding the &#8216;lib&#8217; and &#8216;.a&#8217; pre- and postfixes</li>
<li><code>skipdirs</code> is a list of directory names to skip when scanning for source files</li>
<li><code>skipfiles</code> is a list of file names to ignore</li>
<li><code>dependson</code> is a list of other targets that must be built before this target gets built </li>
<li><code>compilegroups</code> is a dictionary containing lists of files corresponding to special sets of compile flags. In the example above, <code>safe</code> is a the name of a compile group, and it contains just the one file <code>HistogramBuilder.f90</code>.</li>
</ul>

<p>An executable target has a few extra keys:</p>

<pre><code>{
    'name' : 'cmc',
    'rootdirs' : ['cmc'],
    'buildsubdir' : 'cmc',
    'libraryname' : 'cmc',
    'skipdirs' : ['CVS', '.svn'],
    'skipfiles' : [],
    'dependson' : ['libbase'],
    'exename' : 'cmc',
    'mainprogramfile' : 'cmc.f90',
    'compilegroups' : {
    }
},
</code></pre>

<p>Note that even an executable target has a <code>libraryname</code> setting; that&#8217;s because all object files get archived in static libraries, even for an executable.</p>

<p>The main difference between the library and executable target dictionaries are the <code>exename</code> and <code>mainprogramfile</code> settings. The <code>exename</code> is the name used for the resulting executable, and <code>mainprogramfile</code> is the name of the source file that contains the main program. (The object file of the main program will not be archived in the static library.)</p>

<p>Build configurations allow you to build targets with different sets of compile settings. For example, they could be used to build separate parallel, serial, and debug versions of a target. A build configuration looks like this</p>

<pre><code>{
    'name' : 'default',
    'buildsubdir' : 'default',
    'compileroptions' : {
        'archivecommand'    : 'ar -r',
        'unarchivecommand'  : 'ar -d',
        'ranlibcommand'     : 'ranlib -s',
        'f77compiler'       : 'gfortran',
        'f90compiler'       : 'gfortran',
        'f77flags'          : '-c -m32 -ffixed-form',
        'f90flags'          : '-c -m32 -ffree-form',
        'modpathoption'     : '-I',
        'ccompiler'         : 'gcc',
        'cflags'            : '-c -m32 -O3 -funroll-loops -malign-natural',
        'link'              : 'gfortran',
        'linkflags'         : '',
        'prioritylibs'      : '',
        'otherlibs'         : '',
        'compilegroupflags' : {
            'default'       : '',
            'safe'          : '-O0'
        }
    }
},
</code></pre>

<p>It is useful, though not compulsory, to define standard build settings in one &#8216;default&#8217; configuration. This configuration never gets built directly, but is used as the basis of other configurations.</p>

<p>The default configuration above should be fairly self explanatory. It defines fairly standard settings, similar to settings you would see in other build tools. However, there are a couple of settings that could use some additional explanation: <code>prioritylibs</code> is a string used in linking to add any external libraries that should appear early in the link command, before any other libraries. Link order can sometimes be significant for resolving symbols, and that is the reason it has been provided. The <code>compilegroupflags</code> dictionary defines <em>extra</em> compile options that are applied to the files included in the corresponding groups declared earlier in the target settings. One noteworthy point is that all build configurations must have a <code>default</code> key in the <code>compilegroupflags</code>, which is used for all files that do not fall into any other group.</p>

<p>The <code>default</code> configuration above is never actually built, but is used to set default values for other build configurations. This works via an &#8216;inheritance&#8217; mechanism, a bit like in object-oriented programming. The <code>release</code> build configuration inherits everything from the <code>default</code> configuration, and <em>overrides</em> a few settings.</p>

<pre><code>{
    'name'                  : 'release',
    'inherits'              : 'default',
    'buildsubdir'           : 'release',
    'installdir'            : '$FORAY_PROJECT_ROOT/bin.release',
    'compileroptions' : {
        'compilegroupflags' : {
            'default'       : '-O3',
            'safe'          : '-O2'
        }
    }
},
</code></pre>

<p>The <code>inherits</code> key gives the name of the inherited configuration. Anything that appears in the <em>derived</em> configuration overrides the value from the inherited configuration. This works in a recursive way. For example, the <code>release</code> build configuration defines a <code>compileoptions</code> dictionary containing one <code>compilegroupflags</code> key. This does not mean that all the settings in the <code>default</code> configuration&#8217;s <code>compileroptions</code> dictionary will be replaced; only the specific ones provided, like the <code>default</code> and <code>safe</code> keys in <code>compilegroupflags</code> will be replaced. Any others would remain intact.</p>

<p>There is one last aspect of the <code>release</code> configuration that demands consideration: the <code>installdir</code> setting. After a successful build, Foray will copy any resulting executable to the install directory. You can set the same install directory for each target, in which case only the executable&#8217;s from the last configuration built will survive afterwards, with all others being overwritten, or you can use a different install directory for each configuration.</p>

<h2 id="building_with_foray">Building with Foray</h2>

<p>Foray is straightforward to use. It must be run from the project root directory. To build all targets, with the default configuration, you can simply issue:</p>

<pre><code>foray
</code></pre>

<p>To build on a multi-core machine, you can set the number of threads using the <code>-j</code> option.</p>

<pre><code>foray -j 2
</code></pre>

<p>(You can also set an environment variable to do the same: <code>FORAY_NUM_THREADS</code>.)</p>

<p>To only build certain targets, just list them (in any order):</p>

<pre><code>foray cmctest cmc
</code></pre>

<p>Any targets that the listed targets depend upon will also be built.</p>

<p>You can also build multiple configurations at once using the <code>-b</code> option.</p>

<pre><code>foray -b release -b debug cmctest cmc
</code></pre>

<p>Each configuration will be build with each target listed.</p>

<p>Finally, there are a few other useful options: <code>-h</code> for help; <code>-d</code> for verbose debugging output; and <code>-c</code> to do a clean build, in which all files are rebuilt.</p>

<h2 id="advanced_usage">Advanced Usage</h2>

<p>The example above shows how a standard Fortran project can be configured for building with Foray. Unfortunately, many projects are more complex. Foray offers a few extra configuration options to address cases where the basic configuration is not enough.</p>

<h4 id="using_a_preprocessor">Using a Preprocessor</h4>

<p>To use a preprocessor for your Fortran source code, you need to be a bit familiar with Python, so that you can supply functions that run the preprocessor. The <code>cmc</code> example supplied with Foray shows how.</p>

<pre><code># Preprocessing functions
def preprocessedfilename(infile):
    """
    Returns the preprocessed file name for the source file name
    passed in.
    """
    base, ext = os.path.splitext(infile)

    if ext == '.d':        
        outfile = base + '.f'
    elif ext == '.d90':
        outfile = base + '.f90'
    else:
        outfile = infile

    return outfile

def preprocess(srcPath, outDir):
    """
    This function demonstrates how you can preprocess source files.
    If you do not need a preprocessor, you can remove this function, and
    the preprocessor related keys from the buildinfo dictionary.
    This example assumes you want to use the C preprocessor for fortran files.
    It expects a source file with extension d or d90, and produces a fortran file 
    on output.
    """
    filename = os.path.basename(srcPath)
    outFile = preprocessedfilename(filename)
    outPath = os.path.join(outDir, outFile)
    return (0 == subprocess.call('gcc -E -x c -P -C "%s" &gt; "%s"' % (srcPath, outPath), shell=True))


# Fortran file types
fortranfiles = \
{
    'freeformregex'             : '.*\.f90$',
    'fixedformregex'            : '.*\.f$',
    'freeformpreprocessregex'   : '.*\.d90$',
    'fixedformpreprocessregex'  : '.*\.d$',
    'preprocessednamefunc'      : preprocessedfilename,
    'preprocessfunc'            : preprocess,
    'includefileregex'          : '.*\.fh$'
}

...

# Combine everything in buildinfo dictionary   
buildinfo = \
{
    'builddir'      : '$FORAY_PROJECT_ROOT/build',
    'fortranfiles'  : fortranfiles,
    'targets'       : targets,
    'defaultconfig' : 'debug',
    'configs'       : configs
}
</code></pre>

<p>You need to write two Python functions in the BuildInfo file. The first takes the name of a yet to be preprocessed file, and returns the name of the file produced by the preprocessor. In other words, this function simply translates a file name prior to preprocessing into the file name after preprocessing.</p>

<p>The second function actually performs the preprocessing. In the example above, the <code>gcc</code> C preprocessor is used. The function takes the path to the source file, and the path to the directory where the preprocessed file should end up, and returns <code>True</code> if preprocessing was successful, and <code>False</code> otherwise. The example above shows that in the most common cases, the function should simply run a command that invokes the preprocessor, and ensure that the output file ends up in the directory passed to the function.</p>

<p>That explains how the functions should be written, but not how they are passed to the Foray tool. The functions need to be added to the <code>buildinfo</code> data structure, just like all of the other configuration options. To do that, you need to write a entry called <code>fortranfiles</code>. The value of this option should be a Python dictionary, which contains the two preprocessing functions, and other entries containing regular expressions that match the file names of the various source file types, as follows</p>

<ul>
<li><code>freeformregex</code>: Should match a standard free-format Fortran file that does not need preprocessing.</li>
<li><code>fixedformregex</code>: Should match a standard fixed-format Fortran file that does not need preprocessing.</li>
<li><code>freeformpreprocessregex</code>: Should match a free-format Fortran file that needs preprocessing.    </li>
<li><code>fixedformpreprocessregex</code>: Should match a fixed-format Fortran file that needs preprocessing.</li>
<li><code>includefileregex</code>: Should match a Fortran file that gets included in other files, but does not need to be compiled.</li>
<li><code>preprocessednamefunc</code>: The function that translates a file name before preprocessing into the file name after preprocessing.</li>
<li><code>preprocessfunc</code>: The Python function that invokes the preprocessor.</li>
<li><code>f90/f77defaultCompileGroup</code>: Optional. Define a default compile group for f90 / f77 files respectively, which can be defined in compilegroupflags entry of the build config. There default value is set to 'default'.</li>
</ul>

<p>There is a block analogous to <code>fortranfiles</code> for C files, called <code>cfiles</code>. Note that if you are only using the standard C preprocessor, you do not need a <code>cfiles</code> block. You only need to add a <code>cfiles</code> block if you want to run a second preprocessor in addition to the standard C preprocessor. The entries in <code>cfiles</code> are</p>

<ul>
<li><code>fileNameRegEx</code>: Should match a standard C file name.</li>
<li><code>includeFileRegEx</code>: Should match a standard C header file name.</li>
<li><code>preprocessFileNameRegEx</code>: Should match a C file that needs to be preprocessed (by the secondary preprocessor).</li>
<li><code>preprocessednamefunc</code>: The function that translates a file name before preprocessing into the file name after preprocessing.</li>
<li><code>preprocessfunc</code>: The Python function that invokes the preprocessor.</li>
<li><code>defaultCompileGroup</code>: Optional compile group name to be defined in the configs in the compilegroupflags entry. Default value is 'default'.</li>
</ul>

<h4 id="performing_setup_on_first_build">Performing Setup on First Build</h4>

<p>In some projects, there may need to be some setup of the build environment on the first build. For example, perhaps certain files are only available in object form, and you thus need to ensure that some of the libraries are populated with these objects before building begins. </p>

<p>Foray provides a hook for this, in the form of a callback function. If you want to perform some setup the first time a particular target and configuration are built, you need to supply a Python function in the buildinfo dictionary, with the key <code>firstbuildfunc</code>. The function should take the following arguments:</p>

<ul>
<li>The project root directory.</li>
<li>The root source directories of the current target.</li>
<li>The directory used to store intermediate build products (<em>eg</em> preprocessed files, module files).</li>
<li>The directory used to store libraries for the current target/config combination.</li>
<li>The directory used to store executables for the current target/config combination.</li>
<li>The install directory for the current target/config combination.</li>
</ul>

<p>You can use any of these arguments to aid your setup.</p>

<h4 id="preparing_to_build_a_particular_configuration">Preparing to Build a Particular Configuration</h4>

<p>Foray also provides a hook for preparing your project to build a particular build configuration. Perhaps certain files need to be swapped in or out when building a particular configuration. </p>

<p>The function should be supplied in the BuildInfo file with the key <code>prepareconfigfunc</code>. It should take one argument, the name of the build configuration. If you need access to other aspects of the build environment, you can access the Foray environment variables (see below).</p>

<h4 id="environment_variables">Environment Variables</h4>

<p>Foray maintains a set of environment variables while it builds.  These can be accessed in your scripts or BuildInfo file. Here is a list of Foray&#8217;s environment variables:</p>

<ul>
<li><code>FORAY_PROJECT_ROOT</code>: The project root directory.</li>
<li><code>FORAY_TARGET_ROOT_DIRS</code>: The source directories of the current target.</li>
<li><code>FORAY_INTERMEDIATE_PRODUCTS_DIR</code>: The directory used to store intermediate build products (<em>eg</em> preprocessed source)</li>
<li><code>FORAY_LIBRARY_PRODUCTS_DIR</code>: The directory used to store product libraries.</li>
<li><code>FORAY_EXECUTABLE_PRODUCTS_DIR</code>: The directory used to store product executables.</li>
<li><code>FORAY_INSTALL_DIR</code>: The install directory for the current build config.</li>
</ul>

<h4 id="debugging_your_buildinfo_file">Debugging Your BuildInfo File</h4>

<p>You can get verbose debugging output from the <code>foray</code> tool by supplying the <code>-d</code> option. You can use the same mechanism to debug your BuildInfo file: Simply import the Python <code>logging</code> module, and add logging statements to BuildInfo, like this</p>

<pre><code>    logging.debug('This is a debugging string')
</code></pre>

<p>Your debugging messages will only appear when the <code>-d</code> option is supplied to Foray.</p>

<h2 id="foray8217s_license">Foray&#8217;s License</h2>

<p>Foray is available under the New BSD license, so do with it as you please, as long as you don&#8217;t claim it to be your own. (The license is at the bottom of the <code>foray</code> script.) The latest version of Foray is available at Google Code.</p>

<h2 id="known_issues">Known Issues</h2>

<p>The following issues will be addressed in future releases:
*   Foray will not yet handle C dependencies. In other words, if you change a C header file, it will not see that files including that header need to be recompiled. For the time being, if you make changes in C headers, you need to touch any files using them.
*   Dependencies arising from Fortran &#8216;include&#8217; statements are not yet handled.
*   It is not yet possible to tell Foray that certain preprocessor statements lead to dependencies. For example, if you use the C preprocessor with your Fortran source files, and have <code>#include</code> directives, Foray will not recognize the implied dependency.</p>
