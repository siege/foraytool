import os.path, subprocess, os, logging, string, platform
from runsubprocess import RunSubprocess

# Structural elements
CONFIGS             = 'configs'
COMPILEROPTIONS     = 'compileroptions'
COMPILEGROUPFLAGS   = 'compilegroupflags'
TARGETS             = 'targets'
COMPILEGROUPS       = 'compilegroups'

# Compile options
PARSERCOMPILER      = 'parsercompiler'
ARCHIVECOMMAND      = 'archivecommand'
UNARCHIVECOMMAND    = 'unarchivecommand'
RANLIBCOMMAND       = 'ranlibcommand'
F77COMPILER         = 'f77compiler'
F90COMPILER         = 'f90compiler'
F77FLAGS            = 'f77flags'
F90FLAGS            = 'f90flags'
MODPATHOPTION       = 'modpathoption'
CCOMPILER           = 'ccompiler'
CFLAGS              = 'cflags'
CPPCOMPILER         = 'cppcompiler'
CPPFLAGS            = 'cppflags'
CUDACOMPILER        = 'cudacompiler'
CUDAFLAGS           = 'cudaflags'
LINK                = 'link'
LINKFLAGS           = 'linkflags'
PRIORITYLIBS        = 'prioritylibs'
OTHERLIBS           = 'otherlibs'
LOWESTPRIOLIBS      = 'lowestpriolibs'
PLATFORM            = 'platform'

# Compile group identifiers
VERYFAST            = 'veryfast'
FAST                = 'fast'
DEFAULT             = 'default'
SAFE                = 'safe'
VERYSAFE            = 'verysafe'
JSON                = 'json'
# Compile group identifiers for C
CVERYFAST           = 'cveryfast'
CFAST               = 'cfast'
CNORMAL             = 'cnormal'
CSAFE               = 'csafe'
CVERYSAFE           = 'cverysafe'

# flags that can be set target-specific
# openMP flags are different for ifort and gcc, so we have two lines
CCOMPFLAGSOPENMP    = 'ccompflagsopenmp'
CPPCOMPFLAGSOPENMP  = 'cppcompflagsopenmp'
FCOMPFLAGSOPENMP    = 'fcompflagsopenmp'
LINKFLAGSOPENMP     = 'linkflagsopenmp'
#openCL is only used in C code
CCOMPFLAGSOPENCL    = 'ccompflagsopencl'
LINKFLAGSOPENCL     = 'linkflagsopencl'
FCOMPFLAGSNBO       = 'fcompflagsnbo'
CCOMPFLAGSNBO       = 'ccompflagsnbo'
LINKFLAGSNBO        = 'linkflagsnbo'
CPPCOMPFLAGSMKM     = 'cppcompflagsmkm'
LINKFLAGSMKM        = 'linkflagsmkm'


# Functions

def GetHiddenFiles(projectRootDir, targetName):
    # This can be used to define a bunch of files that should not be unarchived when they are removed 
    # from the project. Very useful for license code, or other routines supplied in object form.
    hiddenFiles = []
    return hiddenFiles

def SetupFunction(projectRootDir, targetRootDirs, intermediatesDir, libDir, exeDir, installDir, adfplatform, parsercompiler, copyLibs=True, resetLibs=False, archiveCommand=None, extractCommand=None):
    # In here you can call things that need to be done before compilation can start, such as unpacking tarbals or copying files
    logging.info( 'Setting up build for target root directory %s' % (targetRootDirs[0]))
    logging.verbose( 'Input flags for SetupFunction: projectRootDir: %s, targetRootDirs: %s, intermediatesDir: %s, libDir: %s, exeDir: %s, installDir: %s, adfplatform: %s, parsercompiler: %s, copyLibs: %s' %(projectRootDir, targetRootDirs, intermediatesDir, libDir, exeDir, installDir, adfplatform, parsercompiler, copyLibs))
    return

def PreprocessedADFFileName(sourceFileName):
    base, ext = os.path.splitext(sourceFileName)
    if ext == '.d':
        newext = '.f'
    elif ext[:2] == '.d':
        newext = '.f90'
    elif ext == '.cd':
        newext = '.c'
    else:
        newext = ext
    return base + newext


def PrepareToBuildConfig(configName):
    return


# Functions for creating parts of the buildinfo data structure  
sourceControlDirs = ['.svn', '.git', 'CVS', 'FFILES', 'OFILES', 'MFILES']
def CreateLibTargetDict(libName, rootDir= None, dependson = [], additionalCompFlags = [],additionalLinkFlags = []):
    if not rootDir:
        rootDir = libName
    return \
        {
            'name' : libName,
            'rootdirs' : [rootDir, os.path.join('src', 'include')],
            'buildsubdir' : libName,
            'libraryname' : libName[3:],
            'skipdirs' : sourceControlDirs,
            'skipfiles' : [],
            'dependson' : dependson,
            'compilegroups' : {},
            'additionalCompFlags' : additionalCompFlags,
            'additionalLinkFlags' : additionalLinkFlags
        }

def CreateExeTargetDict(exeName, mainprogramfile = None, rootDir= None, dependson = None, additionalCompFlags = [],additionalLinkFlags = []):
    if mainprogramfile:
        mainfile = mainprogramfile
    else:
        mainfile = exeName + '.f90'
    if not rootDir:
        rootDir = exeName
    if dependson:
        myDependencies = dependson
    else:
        myDependencies = ['libscm_core']
    extension = ".exe"
    d = \
        {
            'name' : exeName,
            'rootdirs' : [rootDir, os.path.join('src', 'include')],
            'buildsubdir' : exeName,
            'libraryname' : exeName,
            'mainprogramfile' : mainfile,
            'exename' : exeName + extension,
            'skipdirs' : sourceControlDirs,
            'skipfiles' : [],
            'dependson' : myDependencies,
            'compilegroups' : {},
            'additionalCompFlags' : additionalCompFlags,
            'additionalLinkFlags' : additionalLinkFlags
        }
    return d

def CreateSpecialTargetDict(specialName, buildFunction, installFunction, rootDir= None, dependson = None, additionalCompFlags = [],additionalLinkFlags = []):
    if not rootDir:
        rootDir = specialName
    if dependson:
        myDependencies = dependson
    else:
        myDependencies = ['libscm_core']
    d = \
        {
            'name' : specialName,
            'rootdirs' : [rootDir, os.path.join('src', 'include')],
            'buildsubdir' : specialName,
            'libraryname' : specialName,
            'mainprogramfile' : specialName,
            'skipdirs' : sourceControlDirs,
            'skipfiles' : [],
            'dependson' : myDependencies,
            'compilegroups' : {},
            'buildfunction' : buildFunction,
            'installfunction' : installFunction,
            'additionalCompFlags' : additionalCompFlags,
            'additionalLinkFlags' : additionalLinkFlags
        }
    return d

def EffectiveValue(options, defaultoptions, key):
    if options.get(key):
        return options[key]
    else:
        return defaultoptions.get(key,'')

def ExtendOptions(options, key, extension):
    global buildinfo
    strippedExt = extension.strip()
    if len(strippedExt) == 0: return
    existingOptions = options.get(key)
    if not existingOptions: 
        # Use the default value
        existingOptions = buildinfo[CONFIGS][0][COMPILEROPTIONS].get(key,'')
    options[key] = existingOptions + ' ' + strippedExt

# Default Build Info
buildinfo = \
{
    'projectroot' : os.path.realpath(os.environ['AMSHOME']),
    'builddir' : os.path.normpath(os.path.join(os.path.realpath(os.environ['AMSHOME']),'build')),
    'fortranfiles' : {
        'freeformregex'             : '.*\.(f90|F90)$',
        'fixedformregex'            : '.*\.(f|F)$',
        'freeformpreprocessregex'   : '.*\.(d90|dmod\d?)$',
        'fixedformpreprocessregex'  : '.*\.d$',
        'preprocessednamefunc'      : PreprocessedADFFileName,
        'preprocessfunc'            : PreprocessADFFile,
        'includefileregex'          : '.*\.(fh|inc)$'
    },
    'cfiles' : {
        'preprocessfileregex'       : '.*\.cd$',
        'preprocessednamefunc'      : PreprocessedADFFileName,
        'preprocessfunc'            : PreprocessADFFile,
    },
    'cppfiles' : {
        'preprocessfileregex'       : '.*\.cd$',
        'preprocessednamefunc'      : PreprocessedADFFileName,
        'preprocessfunc'            : PreprocessADFFile,
    },
    'firstbuildfunc' : SetupFunction,
    'prepareconfigfunc' : PrepareToBuildConfig,
    'hiddenfilesfunc' : GetHiddenFiles,
    'targets' : [
        # == Libraries ==
        # Foray now inherits dependencies, so if a target depends in libscm_adf there is no need to
        #   specify libscm_dft and libscm_core as dependencies.
        CreateLibTargetDict('libjson-fortran', rootDir=os.path.join('ext','json-fortran')),
        CreateLibTargetDict('libftl'         , rootDir=os.path.join('ext','ftl')),
        CreateLibTargetDict('libscm_core'    , rootDir=os.path.join('src','lib','core'), dependson=['libjson-fortran','libftl']),
        CreateLibTargetDict('libscm_reaxff'  , rootDir=os.path.join('src','lib','reaxff'), dependson=['libscm_core'], additionalCompFlags=[CCOMPFLAGSOPENMP, FCOMPFLAGSOPENMP]),
        # == Executables ==
        # Main programs:
        CreateExeTargetDict('reaxff'        , 'reaxff.f90'           , rootDir=os.path.join('src','reaxff')                  , dependson=['libscm_reaxff'], additionalLinkFlags=[LINKFLAGSOPENMP], additionalCompFlags=[CCOMPFLAGSOPENMP, FCOMPFLAGSOPENMP]),
    ],
    'defaultconfig' : 'default',
    'configs' : [],
    'aliases' : {
    # here you can define aliasses for lists of compilation targets.
    'libtc'    : ['libscm_core'],
    # post_run triggers only the functions defined in foray:post_run()
    'post_run' : []
    }
}

def AddBuildConfig(name, makeDefaultConfig=False, inheritsFrom=''):
    newConfig = {
        'name'                  : name,
        'buildsubdir'           : name,
        'installdir'            : os.path.join(os.environ['AMSHOME'], 'bin.' + name),
        'compileroptions' : {
            'compilegroupflags' : {
            } ,
            'compilegroups' : {
            }
        }
    }
    buildinfo['configs'].append(newConfig)
    if inheritsFrom != '': buildinfo['configs'][-1]['inherits'] = inheritsFrom
    if makeDefaultConfig: buildinfo['defaultconfig'] = name
    return newConfig

def BuildConfig(name):
    return [c for c in buildinfo['configs'] if c['name'] == name][0]


# ------------------------- BSD 3-clause License ----------------------------------------
# Copyright 2020 Software for Chemistry & Materials (SCM) www.scm.com

# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation and/or 
#    other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may 
#    be used to endorse or promote products derived from this software without 
#    specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
# POSSIBILITY OF SUCH DAMAGE.

