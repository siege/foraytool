#  ------------------------------------------------------------------------------------------
#  This file contains the sourcefiles that should be compiled with non-default optimize flags
#  ------------------------------------------------------------------------------------------
compileTweaksDict = {}

# these will match against any target in the project
compileTweaksDict['global'] = {}
groups = compileTweaksDict['global']
groups[VERYSAFE] = ['somesourcefilename.f90', 'wedonotusepathshere.f90']
groups[SAFE] = ['andanotherone.f90']
groups[FAST] = ['thisfilecanbebuildwithoptimizations.f90']
# and now a set that only matches stuff in "target1"
compileTweaksDict['target1'] = {}
groups = compileTweaksDict['target1']
groups[VERYSAFE] = ['justafile.f90']
