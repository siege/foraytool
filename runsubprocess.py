#!/usr/bin/python

def RunSubprocess(command, workdir=None, mergeERRwithOUT=False, timeout=None):
    import subprocess, sys, logging, platform
    success = True
    output = ""
    error = ""
    mystderr = subprocess.PIPE
    if mergeERRwithOUT: mystderr = subprocess.STDOUT
    try:
        if workdir:
            process = subprocess.Popen(command, shell=True, cwd=workdir, stderr=mystderr, stdout=subprocess.PIPE)
        else:
            process = subprocess.Popen(command, shell=True, stderr=mystderr, stdout=subprocess.PIPE)
        if sys.version[0] == '2' or timeout == None:
            output, error = process.communicate()
        else:
            try:
                output, error = process.communicate(timeout=timeout)
            except subprocess.TimeoutExpired:
                logging.warning('Process "%s" timed out after %s seconds and will be killed' %(command, timeout))
                try:
                    import psutil
                except Exception as e:
                    logging.warning('psutil module not found, which is needed to kill all child processes')
                    logging.warning(e)
                else:
                    psproc = psutil.Process(process.pid)
                    for childproc in psproc.children(recursive=True):
                        try:
                            if "windows" in platform.system().lower():
                                # msys-child procs on windows don't show on the psutil process tree
                                msyskill(childproc.pid)
                            childproc.kill()
                        except Exception as e:
                                logging.warning('failed to kill child process of "%s"' %(command))
                                logging.warning(e)
                process.kill()
                # this timeout should never be needed, but it is better to crash than to hang
                output, error = process.communicate(timeout=timeout)
        output = output.replace(b'\r\n',b'\n').decode('utf-8', errors='replace')
        if isinstance(error, bytes):
            error = error.replace(b'\r\n',b'\n').decode('utf-8', errors='replace')
    except OSError as e:
        success = False
        print(e)
    if success:
        if process.returncode < 0:
            if mergeERRwithOUT:
                logging.warning('Subprocess failed.  \n   command: %s \n   output: %s' %(command,output))
            else:
                logging.warning('Subprocess failed.  \n   command: %s \n   output: %s \n   error: %s' %(command,output,error))
            success = False
        elif process.returncode > 0:
            success = False
    return (success, output, error)


# helper subroutines for when using msys on Windows
def getbashkids(pslist, bashpid):
    bashkids = []
    for ps in pslist:
        if ps['bashppid'] == bashpid:
            bashkids.append(ps)
            bashkids += getbashkids(pslist, ps['bashpid'])
    return bashkids

def getmsyspslist():
    import subprocess, logging
    pslist = []
    try:
        ps = subprocess.run('ps', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except Exception as e:
        logging.warning('failed to run "ps" command')
        logging.warning(e)
    else:
        if ps.returncode != 0:
            logging.warning('ps command returned non-zero exit status\n   output: %s \n   error: %s' %(ps.stdout, ps.stderr))
        else:
            psout = ''
            try:
                psout = ps.stdout.replace(b'\r\n',b'\n').decode('utf-8', errors='replace')
                for line in psout.strip().split('\n')[1:]:
                    lijst = line.split()
                    pslist.append({"bashpid":int(lijst[0]), "bashppid":int(lijst[1]), "winpid":int(lijst[3]), "cmd":lijst[7]})
            except Exception as e:
                logging.warning('failed to convert output of the ps command')
                logging.warning(psout)
                logging.warning(e)
    return pslist

def msyskill(winpid):
    import logging, os, signal
    killlist = []
    bashpid = 0
    pslist = getmsyspslist()
    for ps in pslist:
        if ps['winpid'] == winpid:
            bashpid = ps['bashpid']
            killlist.append(ps)
    if bashpid == 0:
        logging.warning("failed to locate bashpid, not killing anything")
        return
    bashkids = getbashkids(pslist, bashpid)
    for kid in bashkids:
        killlist.append(kid)
    for ps in killlist:
        logging.warning("killing an msys child process: %s" %(ps['cmd']))
        os.kill(ps['winpid'], signal.SIGINT)


# ------------------------- BSD 3-clause License ----------------------------------------
# Copyright 2020 Software for Chemistry & Materials (SCM) www.scm.com

# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, 
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, 
#    this list of conditions and the following disclaimer in the documentation and/or 
#    other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may 
#    be used to endorse or promote products derived from this software without 
#    specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
# POSSIBILITY OF SUCH DAMAGE.

